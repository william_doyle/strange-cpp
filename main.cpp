

#include <iostream>

class base {

	public:
		int value = 0;
		base &operator ++(){
			std::cout << "prefix\n";
			this->value += 1;
			return *this;
		};

		base operator ++(int){			//  'int' indicates that this is the postfix operator. 
			std::cout << "postfix\n";
			this->value += 1;
			return *this;
		};

};

int main () {

	base b;
	b++;
	++b;

	return 0;

};
